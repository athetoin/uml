package com._4meonweb.uml.classification;

import com._4meonweb.primitivetypes.UmlBoolean;
import com._4meonweb.primitivetypes.UmlBooleanStatic;
import com._4meonweb.uml.commonstructures.NamedElement;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/** UML Redefinable Element.
 *
 * @author Maxim Rodyushkin */
public interface RedefinableElement extends NamedElement {
  default Stream<RedefinableElement> getRedefinedElements() {
    return Stream.empty();
  }

  default Stream<Classifier> getRedefinitionContexts() {
    return Stream.empty();
  }

  default UmlBoolean isConsistentWith(
      final RedefinableElement redefiningElement) {
    return UmlBooleanStatic.FALSE;
  }

  default UmlBoolean isLeaf() {
    return UmlBooleanStatic.FALSE;
  }

  default UmlBoolean isRedefinitionContextValid(
      final RedefinableElement redefinedElement) {
    return this.makeUmlBoolean(this.getRedefinitionContexts().anyMatch(cntt -> {
      final var cnts = redefinedElement.getRedefinitionContexts()
          .collect(Collectors.toSet());
      final var prns = cntt.getAllParents().collect(Collectors.toSet());
      return prns.containsAll(cnts);
    }));
  }
}
