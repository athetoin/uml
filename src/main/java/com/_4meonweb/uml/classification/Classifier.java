package com._4meonweb.uml.classification;

import com._4meonweb.primitivetypes.UmlBoolean;
import com._4meonweb.uml.commonstructures.Namespace;
import com._4meonweb.uml.commonstructures.TemplateableElement;
import com._4meonweb.uml.commonstructures.UmlType;
import com._4meonweb.uml.structuredclassifier.Property;

import java.util.stream.Stream;

/** A Classifier represents a classification of instances according to their
 * Features.
 *
 * @author Maxim Rodyushkin */
public interface Classifier
    extends Namespace, UmlType, TemplateableElement, RedefinableElement {

  default Stream<Classifier> getAllParents() {
    return Stream.empty();
  }

  /** All of the Properties that are direct (i.e., not inherited or imported)
   * attributes of the Classifier.
   *
   * @return the stream of properties */
  default <P extends Property> Stream<P> getAttributes() {
    return Stream.empty();
  }

  /** If true, the Classifier can only be instantiated by instantiating one of
   * its specializations. An abstract Classifier is intended to be used by other
   * Classifiers e.g., as the target of Associations or Generalizations.
   *
   * @return the flag It is FALSE by default. */
  default UmlBoolean isAbstract() {
    return null;
  }

  /** If true, the Classifier cannot be specialized.
   *
   * @return the flag It is FALSE by default. */
  default UmlBoolean isFinalSpecialization() {
    return null;
  }
}
