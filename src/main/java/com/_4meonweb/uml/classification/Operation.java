package com._4meonweb.uml.classification;

import com._4meonweb.uml.commonstructures.ParameterableElement;
import com._4meonweb.uml.commonstructures.TemplateableElement;

/** An Operation is a BehavioralFeature of a Classifier that specifies the name,
 * type, parameters, and constraints for invoking an associated Behavior. An
 * Operation may invoke both the execution of method behaviors as well as other
 * behavioral responses. Operation specializes TemplateableElement in order to
 * support specification of template operations and bound operations. Operation
 * specializes ParameterableElement to specify that an operation can be exposed
 * as a formal template parameter, and provided as an actual parameter in a
 * binding of a template. */
public interface Operation
    extends TemplateableElement, ParameterableElement, BehavioralFeature {
  // TODO: define members
}
