package com._4meonweb.uml.classification;

import com._4meonweb.uml.commonstructures.Namespace;

/** A BehavioralFeature is a feature of a Classifier that specifies an aspect of
 * the behavior of its instances. A BehavioralFeature is implemented (realized)
 * by a Behavior. A BehavioralFeature specifies that a Classifier will respond
 * to a designated request by invoking its implementing method. */
public interface BehavioralFeature extends Feature, Namespace {
  // TODO: define members
}
