package com._4meonweb.uml.classification;

import com._4meonweb.uml.commonstructures.MultiplicityElement;
import com._4meonweb.uml.commonstructures.TypedElement;

/** A StructuralFeature is a typed feature of a Classifier that specifies the
 * structure of instances of the Classifier.
 *
 * @author Maxim Rodyushkin */
public interface StructuralFeature
    extends MultiplicityElement, TypedElement, Feature {
  // TODO add members
}
