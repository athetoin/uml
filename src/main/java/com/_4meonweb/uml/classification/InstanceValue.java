package com._4meonweb.uml.classification;

import com._4meonweb.uml.values.ValueSpecification;

/** An InstanceValue is a ValueSpecification that identifies an instance.
 *
 * @author Maxim Rodyushkin */
public interface InstanceValue extends ValueSpecification {
  // TODO define
}
