package com._4meonweb.uml.classification;

/** A BehavioredClassifier may have InterfaceRealizations, and owns a set of
 * Behaviors one of which may specify the behavior of the BehavioredClassifier
 * itself.
 *
 * @author Maxim Rodyushkin */
public interface BehavioredClassifier extends Classifier {
  // TODO add members
}
