package com._4meonweb.uml.classification;

/** A StructuralFeature is a typed feature of a Classifier that specifies the
 * structure of instances of the Classifier.
 *
 * @author Maxim Rodyushkin */
public interface Feature {
  // TODO add members
}
