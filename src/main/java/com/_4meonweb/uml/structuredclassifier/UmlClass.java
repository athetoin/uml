package com._4meonweb.uml.structuredclassifier;

import com._4meonweb.uml.classification.BehavioredClassifier;

/** A Class classifies a set of objects and specifies the features that
 * characterize the structure and behavior of those objects. A Class may have an
 * internal structure and Ports.
 *
 * @author Maxim Rodyushkin */
public interface UmlClass extends BehavioredClassifier, EncapsulatedClassifier {
  // TODO add members
}
