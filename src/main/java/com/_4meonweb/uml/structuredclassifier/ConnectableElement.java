package com._4meonweb.uml.structuredclassifier;

import com._4meonweb.uml.commonstructures.ParameterableElement;
import com._4meonweb.uml.commonstructures.TypedElement;

/** ConnectableElement is an abstract metaclass representing a set of instances
 * that play roles of a StructuredClassifier. ConnectableElements may be joined
 * by attached Connectors and specify configurations of linked instances to be
 * created within an instance of the containing StructuredClassifier.
 *
 * @author Maxim Rodyushkin */
public interface ConnectableElement extends TypedElement, ParameterableElement {
  // TODO add members
}