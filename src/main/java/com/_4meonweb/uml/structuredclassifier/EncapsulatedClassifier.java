package com._4meonweb.uml.structuredclassifier;

/** An EncapsulatedClassifier may own Ports to specify typed interaction points.
 *
 * @author Maxim Rodyushkin */
public interface EncapsulatedClassifier extends StructuredClassifier {
  // TODO add members
}
