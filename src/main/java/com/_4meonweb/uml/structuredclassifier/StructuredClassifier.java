package com._4meonweb.uml.structuredclassifier;

import com._4meonweb.uml.classification.Classifier;

/** StructuredClassifiers may contain an internal structure of connected
 * elements each of which plays a role in the overall Behavior modeled by the
 * StructuredClassifier.
 *
 * @author Maxim Rodyushkin */
public interface StructuredClassifier extends Classifier {
  // TODO add members
}
