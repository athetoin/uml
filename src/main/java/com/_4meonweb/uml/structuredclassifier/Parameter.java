package com._4meonweb.uml.structuredclassifier;

import com._4meonweb.uml.commonstructures.MultiplicityElement;

/** It is a specification of an argument used to pass information into or out of
 * an invocation of a BehavioralFeature.
 *
 * <p>Parameters can be treated as ConnectableElements within Collaborations.
 *
 * <p>Moved from Classification due package reference cycle
 *
 * @author Maxim Rodyushkin */
public interface Parameter extends MultiplicityElement, ConnectableElement {
  // TODO define members
}
