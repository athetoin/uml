package com._4meonweb.uml.structuredclassifier;

import com._4meonweb.primitivetypes.UmlBoolean;
import com._4meonweb.primitivetypes.UmlBooleanStatic;
import com._4meonweb.uml.classification.RedefinableElement;
import com._4meonweb.uml.classification.StructuralFeature;
import com._4meonweb.uml.commonstructures.UmlType;
import com._4meonweb.uml.deployments.DeploymentTarget;
import com._4meonweb.uml.simpleclassifiers.DataType;
import com._4meonweb.uml.simpleclassifiers.Interface;
import com._4meonweb.uml.values.ValueSpecification;

import java.util.Optional;
import java.util.stream.Stream;

/** A Property is a StructuralFeature.
 *
 * <p>A Property related by ownedAttribute to a Classifier (other than an
 * association) represents an attribute and might also represent an association
 * end. It relates an instance of the Classifier to a value or set of values of
 * the type of the attribute. A Property related by memberEnd to an Association
 * represents an end of the Association. The type of the Property is the type of
 * the end of the Association. A Property has the capability of being a
 * DeploymentTarget in a Deployment relationship. This enables modeling the
 * deployment to hierarchical nodes that have Properties functioning as internal
 * parts. Property specializes ParameterableElement to specify that a Property
 * can be exposed as a formal template parameter, and provided as an actual
 * parameter in a binding of a template.
 *
 * <p>Moved from Structured Classifiers due package reference cycle.
 *
 * @author Maxim Rodyushkin */
public interface Property
    extends ConnectableElement, DeploymentTarget, StructuralFeature {
  /** The Association of which this Property is a member, if any.
   *
   * @return the association */
  default <A extends Association> Optional<A> getAssociation() {
    return Optional.empty();
  }

  /** Designates the optional association end that owns a qualifier attribute.
   *
   * @return the property */
  default <P extends Property> Optional<P> getAssociationEnd() {
    return Optional.empty();
  }

  /** The DataType that owns this Property, if any.
   *
   * @return the data type */
  default Optional<DataType> getDataType() {
    return Optional.empty();
  }

  /** A ValueSpecification that is evaluated to give a default value for the
   * Property when an instance of the owning Classifier is instantiated.
   *
   * @return the value */
  default Optional<ValueSpecification> getDefaultValue() {
    return Optional.empty();
  }

  /** The Interface that owns this Property, if any.
   *
   * @return the interface */
  default Optional<Interface> getInterface() {
    return Optional.empty();
  }

  /** In the case where the Property is one end of a binary association this
   * gives the other end.
   *
   * <p>{@code body: if association <> null and association.memberEnd->size() = 2
    then
      association.memberEnd->any(e | e <> self)
    else
      null
    endif}
   *
   * @return the property */
  default <P extends Property> Optional<P> getOpposite() {
    return Optional.empty();
  }

  /** The owning association of this property, if any.
   *
   * @return the association */
  default Optional<Association> getOwningAssociation() {
    return Optional.empty();
  }

  /** An optional list of ordered qualifier attributes for the end.
   *
   * @return the properties */
  default <P extends Property> Stream<P> getQualifiers() {
    return Stream.empty();
  }

  /** The properties that are redefined by this property, if any.
   *
   * @return the properties */
  default <P extends Property> Stream<P> getRedefinedProperties() {
    return Stream.empty();
  }

  /** The properties of which this Property is constrained to be a subset, if
   * any.
   *
   * @return the properties */
  default <P extends Property> Stream<P> getSubsettedProperties() {
    return Stream.empty();
  }

  /** The query subsettingContext() gives the context for subsetting a Property.
   * It consists, in the case of an attribute, of the corresponding Classifier,
   * and in the case of an association end, all of the Classifiers at the other
   * ends.
   *
   * <p>{@code body: if association <> null
  then association.memberEnd->excluding(self)->collect(type)->asSet() else if
   * classifier<>null then classifier->asSet() else Set{} endif endif} */
  default <T extends UmlType> Optional<T> getSubsettingContext() {
    return Optional.empty();
  }

  /** The Class that owns this Property, if any.
   *
   * @return the class */
  default <C extends UmlClass> Optional<C> getUmlClass() {
    return Optional.empty();
  }

  /** The query isAttribute() is true if the Property is defined as an attribute
   * of some Classifier.
   *
   * <p>{@code body: not classifier->isEmpty()}
   *
   * @return the attribute flag It is FALSE by default. */
  default UmlBoolean isAttribute() {
    return UmlBooleanStatic.FALSE;
  }

  /** A binding of a PropertyTemplateParameter representing an attribute must be
   * to an attribute.
   *
   * <p>{@code inv: (self.isAttribute()
  and (templateParameterSubstitution->notEmpty())
  implies (templateParameterSubstitution->forAll(ts |
  ts.formal.oclIsKindOf(Property)
  and ts.formal.oclAsType(Property).isAttribute())))}
   *
   * @return the invariant It is FALSE by default. */
  default UmlBoolean isBindingToAttribute() {
    return UmlBooleanStatic.FALSE;
  }

  /** If isComposite is true, the object containing the attribute is a container
   * for the object or value contained in the attribute.
   *
   * <p>This is a derived value, indicating whether the aggregation of the
   * Property is composite or not.
   *
   * <p>{@code body: aggregation = AggregationKind::composite}
   *
   * @return the composite flag It is FALSE by default. */
  default UmlBoolean isComposite() {
    return UmlBooleanStatic.FALSE;
  }

  /** The query isConsistentWith() specifies, for any two Properties in a
   * context in which redefinition is possible, whether redefinition would be
   * logically consistent.
   *
   * <p>A redefining Property is consistent with a redefined Property if the
   * type of the redefining Property conforms to the type of the redefined
   * Property, and the multiplicity of the redefining Property (if specified) is
   * contained in the multiplicity of the redefined Property.
   *
   * {@code
  pre: redefiningElement.isRedefinitionContextValid(self)
   body: redefiningElement.oclIsKindOf(Property) and
   let prop : Property = redefiningElement.oclAsType(Property) in
   (prop.type.conformsTo(self.type) and
   ((prop.lowerBound()->notEmpty() and self.lowerBound()->notEmpty()) implies
    prop.lowerBound() >= self.lowerBound()) and
    ((prop.upperBound()->notEmpty() and self.upperBound()->notEmpty()) implies
    prop.lowerBound() <= self.lowerBound()) and
    (self.isComposite implies prop.isComposite))} */
  default UmlBoolean isConsistedWith(
      final RedefinableElement redefiningElement) {
    return UmlBooleanStatic.FALSE;
  }

  /** A Property can be a DeploymentTarget if it is a kind of Node and functions
   * as a part in the internal structure of an encompassing Node.
   *
   * <p>{@code inv: deployment->notEmpty() implies owner.oclIsKindOf(Node) and Node.allInstances()-
  >exists(n | n.part->exists(p | p = self))}
   *
   * @return the invariant */
  default UmlBoolean isDeploymentTarget() {
    return UmlBooleanStatic.FALSE;
  }

  /** Specifies whether the Property is derived, i.e., whether its value or
   * values can be computed from other information.
   *
   * @return the derived flag */
  default UmlBoolean isDerived() {
    return UmlBooleanStatic.FALSE;
  }

  /** Specifies whether the property is derived as the union of all of the
   * Properties that are constrained to subset it.
   *
   * @return the derived union flag */
  default UmlBoolean isDerivedUnion() {
    return UmlBooleanStatic.FALSE;
  }

  /** A derived union is derived.
   *
   * <p>{@code inv: isDerivedUnion implies isDerived}
   *
   * @return the invariant */
  default UmlBoolean isDerivedUnionDerived() {
    return UmlBooleanStatic.FALSE;
  }

  /** A derived union is read only.
   *
   * <p>{@code inv: isDerivedUnion implies isReadOnly} */
  default UmlBoolean isDerivedUnionReadOnly() {
    return UmlBooleanStatic.FALSE;
  }

  /** Specifies whether the property is derived as the union of all of the
   * Properties that are constrained to subset it.
   *
   * @return the ID flag */
  default UmlBoolean isId() {
    return UmlBooleanStatic.FALSE;
  }

  /** A multiplicity on the composing end of a composite aggregation must not
   * have an upper bound greater than 1.
   *
   * <p>{@code inv: isComposite and association <> null implies opposite.upperBound() <= 1} */
  default UmlBoolean isMultiplicityComposite() {
    return UmlBooleanStatic.FALSE;
  }

  /** The query isNavigable() indicates whether it is possible to navigate
   * across the property.
   *
   * <p>{@code body: not classifier->isEmpty() or association.navigableOwnedEnd->includes(self)} */
  default UmlBoolean isNavigable() {
    return UmlBooleanStatic.FALSE;
  }

  /** All qualified Properties must be Association ends.
   *
   * <p>{@code inv: qualifier->notEmpty() implies association->notEmpty()}
   *
   * @return the invariant */
  default UmlBoolean isQualifiedAssociationEnd() {
    return UmlBooleanStatic.FALSE;
  }

  /** A redefined Property must be inherited from a more general Classifier.
   *
   * <p>{@code inv: (redefinedProperty->notEmpty()) implies
  (redefinitionContext->notEmpty() and
  redefinedProperty->forAll(rp|
  ((redefinitionContext->collect(fc|
  fc.allParents()))->asSet())->collect(c| c.allFeatures())->asSet()->includes(rp)))}
   *
   * @return the invariant */
  default UmlBoolean isRedefinedPropertInherited() {
    return UmlBooleanStatic.FALSE;
  }

  /** A Property may not subset a Property with the same name.
   *
   * <p>{@code inv: subsettedProperty->forAll(sp | sp.name <> name)}
   *
   * @return the invariant */
  default UmlBoolean isSubsettedPropertyName() {
    return UmlBooleanStatic.FALSE;
  }

  /** Subsetting may only occur when the context of the subsetting property
   * conforms to the context of the subsetted property.
   *
   * <p>{@code inv: subsettedProperty->notEmpty() implies
  (subsettingContext()->notEmpty() and subsettingContext()->forAll (sc |
  subsettedProperty->forAll(sp |
  sp.subsettingContext()->exists(c | sc.conformsTo(c)))))} */
  default UmlBoolean isSubsettingContextConforms() {
    return UmlBooleanStatic.FALSE;
  }

  /** A subsetting Property may strengthen the type of the subsetted Property,
   * and its upper bound may be less.
   *
   * <p>{@code inv: subsettedProperty->forAll(sp |
  self.type.conformsTo(sp.type) and
  ((self.upperBound()->notEmpty() and sp.upperBound()->notEmpty()) implies
  self.upperBound() <= sp.upperBound() ))}
   *
   * @return the invariant */
  default UmlBoolean isSubsettingRules() {
    return UmlBooleanStatic.FALSE;
  }

  /** If a Property is a classifier-owned end of a binary Association, its owner
   * must be the type of the opposite end.
   *
   * <p>{@code inv: (opposite->notEmpty() and owningAssociation->isEmpty()) implies classifier =
  opposite.type}
   *
   * @return the invariant */
  default UmlBoolean isTypeOfOppositeEnd() {
    return UmlBooleanStatic.FALSE;
  }
}
