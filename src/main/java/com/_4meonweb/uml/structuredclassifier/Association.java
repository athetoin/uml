package com._4meonweb.uml.structuredclassifier;

import com._4meonweb.primitivetypes.UmlBoolean;
import com._4meonweb.uml.classification.Classifier;
import com._4meonweb.uml.commonstructures.Relationship;
import com._4meonweb.uml.commonstructures.UmlType;

import java.util.stream.Stream;

/** A link is a tuple of values that refer to typed objects. An Association
 * classifies a set of links, each of which is an instance of the Association.
 * Each value in the link refers to an instance of the type of the corresponding
 * end of the Association.
 *
 * @author Maxim Rodyushkin */
public interface Association extends Relationship, Classifier {
  // Operations
  default <T extends UmlType> Stream<T> endType() {
    return Stream.empty();
  }

  // Association Ends
  default <P extends Property> Stream<P> getMemberEnd() {
    return Stream.empty();
  }

  // Attributes
  UmlBoolean isDerived();
}
