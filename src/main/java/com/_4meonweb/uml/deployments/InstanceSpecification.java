package com._4meonweb.uml.deployments;

import com._4meonweb.uml.classification.Classifier;
import com._4meonweb.uml.classification.Slot;
import com._4meonweb.uml.commonstructures.PackageableElement;
import com._4meonweb.uml.values.ValueSpecification;

import java.lang.ref.Reference;
import java.util.Optional;
import java.util.stream.Stream;

/** An InstanceSpecification is a model element that represents an instance in a
 * modeled system.
 *
 * <p>An InstanceSpecification can act as a DeploymentTarget in a Deployment
 * relationship, in the case that it represents an instance of a Node. It can
 * also act as a DeployedArtifact, if it represents an instance of an Artifact.
 *
 * <p>Moved from UML Classification due package reference cycle.
 *
 * @author Maxim Rodyushkin */
public interface InstanceSpecification
    extends DeploymentTarget, PackageableElement, DeployedArtifact {
  /** The Classifier or Classifiers of the represented instance. If multiple
   * Classifiers are specified, the instance is classified by all of them.
   *
   * @return the classifiers */
  default <C extends Classifier> Stream<Reference<C>> getClassifiers() {
    return Stream.empty();
  }

  /** A Slot giving the value or values of a StructuralFeature of the instance.
   *
   * <p>An InstanceSpecification can have one Slot per StructuralFeature of its
   * Classifiers, including inherited features. It is not necessary to model a
   * Slot for every StructuralFeature, in which case the InstanceSpecification
   * is a partial description.
   *
   * @return the slots */
  default <S extends Slot> Stream<S> getSlots() {
    return Stream.empty();
  }

  /** A specification of how to compute, derive, or construct the instance.
   *
   * @return the value specification */
  default <S extends ValueSpecification> Optional<S> getSpecification() {
    return Optional.empty();
  }
  // TODO define constraint
}
