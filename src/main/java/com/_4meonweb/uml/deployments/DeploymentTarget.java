package com._4meonweb.uml.deployments;

import com._4meonweb.uml.commonstructures.NamedElement;

/** A deployment target is the location for a deployed artifact.
 *
 * @author Maxim Rodyushkin */
public interface DeploymentTarget extends NamedElement {
  // TODO add members
}
