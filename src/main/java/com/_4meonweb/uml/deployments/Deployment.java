package com._4meonweb.uml.deployments;

import com._4meonweb.uml.commonstructures.Dependency;

/** UML Deployment.
 *
 * @author Maxim Rodyushkin */
public interface Deployment extends Dependency {
  // TODO define
}
