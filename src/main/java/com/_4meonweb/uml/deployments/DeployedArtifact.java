package com._4meonweb.uml.deployments;

import com._4meonweb.uml.commonstructures.NamedElement;

/** UML Deployed Artifact.
 *
 * @author Maxim Rodyushkin */
public interface DeployedArtifact extends NamedElement {
  // TODO define
}
