package com._4meonweb.uml.deployments;

import com._4meonweb.uml.classification.Classifier;

/** UML Artifact.
 *
 * @author Maxim Rodyushkin */
public interface Artifact extends Classifier, DeployedArtifact {
  // TODO define
}
