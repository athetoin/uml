package com._4meonweb.uml.values;

/** A LiteralSpecification identifies a literal constant being modeled.
 *
 * @author Maxim Rodyushkin */
public interface LiteralSpecification extends ValueSpecification {

}
