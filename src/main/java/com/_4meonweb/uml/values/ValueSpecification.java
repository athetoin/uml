package com._4meonweb.uml.values;

import com._4meonweb.primitivetypes.UmlBoolean;
import com._4meonweb.primitivetypes.UmlBooleanStatic;
import com._4meonweb.primitivetypes.UmlInteger;
import com._4meonweb.primitivetypes.UmlReal;
import com._4meonweb.primitivetypes.UmlString;
import com._4meonweb.primitivetypes.UnlimitedNatural;
import com._4meonweb.uml.commonstructures.PackageableElement;
import com._4meonweb.uml.commonstructures.ParameterableElement;
import com._4meonweb.uml.commonstructures.TypedElement;

import java.util.Optional;

/** A ValueSpecification is the specification of a (possibly empty) set of
 * values.
 *
 * <p>A ValueSpecification is a ParameterableElement that may be exposed as a
 * formal TemplateParameter and provided as the actual parameter in the binding
 * of a template.
 *
 * @author Maxim Rodyushkin */
public interface ValueSpecification extends PackageableElement, TypedElement {

  /** Gets a single Boolean value when one can be computed.
   *
   * @return the value */
  default Optional<UmlBoolean> getBooleanValue() {
    return Optional.empty();
  }

  /** Gets a single Integer value when one can be computed.
   *
   * @return the value */
  default Optional<UmlInteger> getIntegerValue() {
    return Optional.empty();
  }

  /** Gets a single Real value when one can be computed.
   *
   * @return the value */
  default Optional<UmlReal> getRealValue() {
    return Optional.empty();
  }

  /** Gives a single String value when one can be computed.
   *
   * @return the value */
  default Optional<UmlString> getStringValue() {
    return Optional.empty();
  }

  /** Gives a single UnlimitedNatural value when one can be computed.
   *
   * @return the value */
  default Optional<UnlimitedNatural> getUnlimitedValue() {
    return Optional.empty();
  }

  /** The query isCompatibleWith() determines if this ValueSpecification is
   * compatible with the specified ParameterableElement.
   *
   * <p>This ValueSpecification is compatible with ParameterableElement p if the
   * kind of this ValueSpecification is the same as or a subtype of the kind of
   * p. Further, if p is a TypedElement, then the type of this
   * ValueSpecification must be conformant with the type of p.
   *
   * <p>body: self.oclIsKindOf(p.oclType()) and (p.oclIsKindOf(TypedElement)
   * implies self.type.conformsTo(p.oclAsType(TypedElement).type))
   *
   * @param element
   *          compared element
   *
   * @return the compatibility flag */
  @Override
  default UmlBoolean isCompatibleWith(final ParameterableElement element) {
    return null;
  }

  /** The query isComputable() determines whether a value specification can be
   * computed in a model.
   *
   * <p>This operation cannot be fully defined in OCL. A conforming
   * implementation is expected to deliver true for this operation for all
   * ValueSpecifications that it can compute, and to compute all of those for
   * which the operation is true. A conforming implementation is expected to be
   * able to compute at least the value of all LiteralSpecifications.
   *
   * @return the computability flag */
  default UmlBoolean isComputable() {
    return UmlBooleanStatic.FALSE;
  }

  /** Gets true when it can be computed that the value is null.
   *
   * @return the null flag */
  default UmlBoolean isNull() {
    return UmlBooleanStatic.FALSE;
  }

}
