package com._4meonweb.uml.values;

import com._4meonweb.primitivetypes.UmlBoolean;
import com._4meonweb.primitivetypes.UmlString;

import java.util.Optional;

/** A LiteralString is a specification of a String value.
 *
 * @author Maxim Rodyushkin */
public interface LiteralString extends LiteralSpecification {

  /** The query stringValue() gives the value.
   *
   * @return the string value */
  @Override
  default Optional<UmlString> getStringValue() {
    return this.getValue();
  }

  /** The specified String value. (Attribute)
   *
   * @return the string value */
  default Optional<UmlString> getValue() {
    return Optional.empty();
  }

  /** The query isComputable() is redefined to be true. (Operation)
   *
   * <p>Redefines ValueSpecification::stringValue()}
   *
   *
   * @return the computability flag */
  @Override
  default UmlBoolean isComputable() {
    return null;
  }
}
