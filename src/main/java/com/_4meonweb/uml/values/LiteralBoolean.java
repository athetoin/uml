package com._4meonweb.uml.values;

import com._4meonweb.primitivetypes.UmlBoolean;
import com._4meonweb.primitivetypes.UmlBooleanStatic;

import java.util.Optional;

/** Specification of a Boolean value.
 *
 * @author Maxim Rodyushkin */
public interface LiteralBoolean extends LiteralSpecification {

  @Override
  default Optional<UmlBoolean> getBooleanValue() {
    return Optional.empty();
  }

  default UmlBoolean getValue() {
    return UmlBooleanStatic.FALSE;
  }

  @Override
  default UmlBoolean isComputable() {
    return UmlBooleanStatic.TRUE;
  }

}
