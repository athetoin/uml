package com._4meonweb.uml.commonstructures;

import com._4meonweb.primitivetypes.UmlBoolean;
import com._4meonweb.primitivetypes.UmlBooleanStatic;

public interface PrimitivePlugin {
  default UmlBoolean makeUmlBoolean(final boolean value) {
    return value ? UmlBooleanStatic.TRUE : UmlBooleanStatic.FALSE;
  }
}
