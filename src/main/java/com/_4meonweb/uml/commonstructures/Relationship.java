package com._4meonweb.uml.commonstructures;

import java.util.stream.Stream;

/** Relationship is an abstract concept that specifies some kind of relationship
 * between Elements.
 *
 * @author Maxim Rodyushkin */
// TODO Define OCL ancestor
public interface Relationship extends Element {
  /** Specifies the elements related by the Relationship.
   *
   * @return elements */
  default <E extends Element> Stream<E> getRelatedElement() {
    return Stream.empty();
  }
}
