package com._4meonweb.uml.commonstructures;

import java.util.stream.Stream;

/** A TemplateBinding is a DirectedRelationship between a TemplateableElement
 * and a template. A TemplateBinding specifies the
 * TemplateParameterSubstitutions of actual parameters for the formal parameters
 * of the template. */
// TODO Define OCL ancestor
public interface TemplateBinding extends DirectRelationship {
  /** Gets the TemplateableElement that is bound by this TemplateBinding.
   *
   * @return the Templateable Element */
  default <E extends TemplateableElement> E getBoundElement() {
    return null;
  }

  /** Gets the TemplateParameterSubstitutions owned by this TemplateBinding.
   *
   * @return the TemplateParameter Substitutions */
  default Stream<TemplateParameterSubstitution> getParameterSubstitution() {
    return Stream.empty();
  }

  /** Gets the TemplateSignature for the template that is the target of this
   * TemplateBinding.
   *
   * @return the Template Signature */
  default <S extends TemplateSignature> S getSignature() {
    return null;
  }
}
