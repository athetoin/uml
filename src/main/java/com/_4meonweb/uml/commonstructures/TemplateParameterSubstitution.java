package com._4meonweb.uml.commonstructures;

import com._4meonweb.primitivetypes.UmlBoolean;

import java.util.Optional;

/** A TemplateParameterSubstitution relates the actual parameter to a formal
 * TemplateParameter as part of a template binding.
 *
 * @author Maxim Rodyushkin */
// TODO Define OCL ancestor
public interface TemplateParameterSubstitution extends Element {
  /** Gets the ParameterableElement that is the actual parameter for this
   * TemplateParameterSubstitution.
   *
   * @return the Parameterable Element */
  default <E extends TemplateableElement> E getActual() {
    return null;
  }

  /** Gets the formal TemplateParameter that is associated with this
   * TemplateParameterSubstitution.
   *
   * @return the formal Template Parameter */
  default <P extends TemplateParameter> P getFormal() {
    return null;
  }

  /** Gets the ParameterableElement that is owned by this
   * TemplateParameterSubstitution as its actual parameter.
   *
   * @return the Parameterable Element */
  default <E extends TemplateableElement> Optional<E> getOwnedActual() {
    return Optional.empty();
  }

  /** Gets the TemplateBinding that owns this TemplateParameterSubstitution.
   *
   * @return the Template Binding */
  default <T extends TemplateBinding> T getTemplateBinding() {
    return null;
  }

  /** The actual ParameterableElement must be compatible with the formal
   * TemplateParameter, e.g., the actual ParameterableElement for a Class
   * TemplateParameter must be a Class.
   *
   * @return It is FALSE by default */
  UmlBoolean mustBeCompatible();
}
