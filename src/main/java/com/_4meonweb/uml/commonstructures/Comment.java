package com._4meonweb.uml.commonstructures;

import com._4meonweb.primitivetypes.UmlString;

import java.util.Optional;
import java.util.stream.Stream;

/** A Comment is a textual annotation that can be attached to a set of Elements.
 *
 * @author Maxim Rodyushkin */
public interface Comment extends Element {
  /** References the Element(s) being commented. (Association)
   *
   * @return the commented element */
  default Stream<Element> getAnnotatedElements() {
    return Stream.empty();
  }

  /** Gets the string that is the comment. (Attribute)
   *
   * @return the comment string */
  default Optional<UmlString> getBody() {
    return Optional.empty();
  }
}
