package com._4meonweb.uml.commonstructures;

import com._4meonweb.primitivetypes.UmlBoolean;

import java.util.Optional;

/** A Type constrains the values represented by a TypedElement.
 *
 * @author Maxim Rodyushkin */
public interface UmlType extends PackageableElement {

  /** Gives true for a Type that conforms to another. By default, two Types do
   * not conform to each other. This query is intended to be redefined for
   * specific conformance situations.
   *
   * @param other
   *          compared type
   * @return the result It is FALSE by default. */
  default UmlBoolean conformsTo(final UmlType other) {
    throw new UnsupportedOperationException();
  }

  /** Specifies the owning Package of this Type, if any.
   *
   * @return the package */
  default <P extends UmlPackage> Optional<P> getPackage() {
    return Optional.empty();
  }
}
