package com._4meonweb.uml.commonstructures;

import java.util.stream.Stream;

/** A DirectedRelationship represents a relationship between a collection of
 * source model Elements and a collection of target model Elements.
 *
 * @author Maxim Rodyushkin */
// TODO Define OCL ancestor
public interface DirectRelationship extends Relationship {
  /** Gets specified the source Element(s) of the DirectedRelationship.
   *
   * @return source elements */
  default Stream<Element> getSources() {
    return Stream.empty();
  }

  /** Gets specified the target Element(s) of the DirectedRelationship.
   *
   * @return target elements */
  default Stream<Element> getTargets() {
    return Stream.empty();
  }
}
