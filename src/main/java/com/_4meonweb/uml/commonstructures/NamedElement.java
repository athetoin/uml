package com._4meonweb.uml.commonstructures;

import com._4meonweb.primitivetypes.UmlBoolean;
import com._4meonweb.primitivetypes.UmlBooleanStatic;
import com._4meonweb.primitivetypes.UmlString;
import com._4meonweb.primitivetypes.UmlStringStatic;
import com._4meonweb.uml.values.StringExpression;

import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/** A NamedElement is an Element in a model that may have a name. The name may
 * be given directly and/or via the use of a StringExpression.
 *
 * @author Maxim Rodyushkin */
public interface NamedElement extends Element {

  /** Gives the sequence of Namespaces in which the NamedElement is nested,
   * working outwards.
   *
   * @return the ordered set of namespaces. */
  default Stream<Namespace> allNameSpaces() {
    return Stream.empty();
  }

  /** Gives the sequence of Namespaces in which the NamedElement is nested,
   * working outwards.
   *
   * @return the namespaces */
  default Stream<Namespace> getAllNamespaces() {
    final var nmsc = this.getNamespace();
    if (nmsc.isPresent()) {
      return Stream.concat(nmsc.stream(), nmsc.get().allNameSpaces());
    }
    return Stream.empty();
  }

  /** Gets the set of all the enclosing Namespaces of this NamedElement, working
   * outwards, that are Packages, up to but not including the first such
   * Namespace that is not a Package.
   *
   * @return the package set */
  default Stream<UmlPackage> getAllOwningPackages() {
    if (this.getNamespace().filter(UmlPackage.class::isInstance).isPresent()) {
      return Stream
          .concat(this.getNamespace().map(UmlPackage.class::cast).stream(),
              this.getNamespace().stream()
                  .flatMap(NamedElement::getAllOwningPackages))
          .distinct();
    }
    return Stream.empty();
  }

  /** Indicates the Dependencies that reference this NamedElement as a client.
   *
   * @return the dependencies */
  default Stream<Dependency> getClientDependencies() {
    return Stream.empty();
  }

  /** Gets the name of the NamedElement.
   *
   * @return the name */
  default Optional<UmlString> getName() {
    return Optional.empty();
  }

  /** Gets the StringExpression used to define the name of this NamedElement.
   *
   * @return the string expression */
  default Optional<StringExpression> getNameExpression() {
    return Optional.empty();
  }

  /** Specifies the Namespace that owns the NamedElement.
   *
   * @return the namespace */
  default Optional<Namespace> getNamespace() {
    return Optional.empty();
  }

  @Override
  default Stream<Element> getOwnedElements() {
    return Stream.concat(Element.super.getOwnedElements(),
        this.getNameExpression().stream()).distinct();
  }

  @Override
  default Optional<Element> getOwner() {
    return this.getNamespace().map(Element.class::cast);
  }

  /** Gets a name that allows the NamedElement to be identified within a
   * hierarchy of nested Namespaces. It is constructed from the names of the
   * containing Namespaces starting at the root of the hierarchy and ending with
   * the name of the NamedElement itself.
   *
   * @return the qualified name */
  default Optional<UmlString> getQualifiedName() {
    if (this.getName().isPresent()) {
      if (this.getNamespace().isPresent() && this.getNamespace()
          .flatMap(Namespace::getQualifiedName).isPresent()) {
        return Optional.of(() -> NamedElement.this.getNamespace()
            .flatMap(Namespace::getQualifiedName).get().getValue()
            + NamedElement.this.getSeparator().getValue()
            + NamedElement.this.getName().get().getValue());
      } else {
        return this.getName();
      }
    }
    return Optional.empty();
  }

  /** The query separator() gives the string that is used to separate names when
   * constructing a qualifiedName.
   *
   * @return the separator. It is '::' by default. */
  default UmlString getSeparator() {
    return UmlStringStatic.SEPARATOR;
  }

  /** Determines whether and how the NamedElement is visible outside its owning
   * Namespace.
   *
   * @return the visibility kind */
  default Optional<VisibilityKind> getVisibility() {
    return Optional.empty();
  }

  /** The query isDistinguishableFrom() determines whether two NamedElements may
   * logically co-exist within a Namespace. By default, two named elements are
   * distinguishable if (a) they have types neither of which is a kind of the
   * other or (b) they have different names. body:
   * (self.oclIsKindOf(n.oclType()) or n.oclIsKindOf(self.oclType())) implies
   * ns.getNamesOfMember(self)->intersection(ns.getNamesOfMember(n))->isEmpty()
   *
   * @param nmdElment
   *          the named element
   * @param ns
   *          the namespace
   * @return the distinguishing flag. It is FALSE by default */
  default UmlBoolean isDistinguishableFrom(final NamedElement nmdElment,
      final Namespace ns) {
    if (this.getClass().isInstance(nmdElment)
        || nmdElment.getClass().isInstance(this)) {
      final var intn = ns.getNamesOfMember(this).collect(Collectors.toSet());
      intn.retainAll(
          ns.getNamesOfMember(nmdElment).collect(Collectors.toSet()));
      return intn::isEmpty;
    }
    return UmlBooleanStatic.TRUE;
  }
}
