/** Constraints of UML packages.
 *
 * @author Maxim Rodyushkin */
package com._4meonweb.uml.commonstructures.constraint;