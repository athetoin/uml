package com._4meonweb.uml.commonstructures.constraint;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Documented
@Retention(RUNTIME)
@Target({ METHOD, ANNOTATION_TYPE })
@Constraint(validatedBy = HasUriFormatValidator.class)
public @interface HasUriFormat {
  /** Validation group.
   *
   * @return the groups */
  Class<?>[] groups() default {};

  /** Validation message.
   *
   * @return the message */
  String message() default "URI has improper format";

  /** Validation payload.
   *
   * @return the payload */
  Class<? extends Payload>[] payload() default {};
}
