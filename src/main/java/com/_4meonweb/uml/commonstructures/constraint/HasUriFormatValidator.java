package com._4meonweb.uml.commonstructures.constraint;

import com._4meonweb.primitivetypes.UmlString;

import java.net.URI;
import java.util.Optional;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class HasUriFormatValidator
    implements ConstraintValidator<HasUriFormat, Object> {

  @SuppressWarnings("unchecked")
  @Override
  public boolean isValid(final Object value,
      final ConstraintValidatorContext context) {
    if (!(value instanceof Optional)) {
      return false;
    }
    try {
      return ((Optional<UmlString>) value).map(UmlString::getValue)
          .map(URI::create).map(URI::isAbsolute).orElse(true);
    } catch (final IllegalArgumentException excn) {
      return false;
    }
  }

}
