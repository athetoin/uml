package com._4meonweb.uml.commonstructures;

import java.util.stream.Stream;

/** UML Dependency.
 *
 * @author Maxim Rodyushkin */
public interface Dependency extends DirectRelationship, PackageableElement {
  default Stream<NamedElement> getClient() {
    return Stream.empty();
  }

  default Stream<NamedElement> getSupplier() {
    return Stream.empty();
  }
}
