package com._4meonweb.uml.commonstructures;

import com._4meonweb.primitivetypes.NaturalStatic;
import com._4meonweb.primitivetypes.UmlBoolean;
import com._4meonweb.primitivetypes.UmlBooleanStatic;
import com._4meonweb.primitivetypes.UmlInteger;
import com._4meonweb.primitivetypes.UmlIntegerStatic;
import com._4meonweb.primitivetypes.UnlimitedNatural;

/** A multiplicity is a definition of an inclusive interval of non-negative
 * integers beginning with a lower bound and ending with a (possibly infinite)
 * upper bound. A MultiplicityElement embeds this information to specify the
 * allowable cardinalities for an instantiation of the Element.
 *
 * @author Maxim Rodyushkin */
public interface MultiplicityElement extends Element {

  /** UML Multiplicity Element builder.
   *
   * @author Maxim Rodyushkin */
  public interface MultiplicityElementMaker<E extends MultiplicityElement,
      M extends MultiplicityElementMaker<E, M>> {

    /** Puts order flag.
     *
     * @param flag
     *          the flag
     * @return this builder */
    default M putOrderedFlag(final UmlBoolean flag) {
      return null;
    }

    /** Puts uniqueness flag.
     *
     * @param flag
     *          the flag
     * @return this builder */
    default M putUniqueness(final UmlBoolean flag) {
      return null;
    }
  }

  /** The lower bound of the multiplicity interval.
   *
   * @return the bound. It is 0 by default. */
  default UmlInteger getLower() {
    return UmlIntegerStatic.ZERO;
  }

  /** The upper bound of the multiplicity interval.
   *
   * @return the bound. It is 1 by default. */
  default UnlimitedNatural getUpper() {
    return NaturalStatic.ONE;
  }

  /** For a multivalued multiplicity, this attribute specifies whether the
   * values in an instantiation of this MultiplicityElement are sequentially
   * ordered.
   *
   * @return the order flag. It is FALSE by default. */
  default UmlBoolean isOrdered() {
    return UmlBooleanStatic.FALSE;
  }

  /** For a multivalued multiplicity, this attribute specifies whether the
   * values in an instantiation of this MultiplicityElement are unique.
   *
   * @return the uniqueness flag. It is TRUE by default */
  default UmlBoolean isUnique() {
    return UmlBooleanStatic.TRUE;
  }
}
