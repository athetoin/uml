package com._4meonweb.uml.commonstructures;

import com._4meonweb.primitivetypes.UmlBoolean;
import com._4meonweb.primitivetypes.UmlBooleanStatic;

import java.util.Optional;

/** A ParameterableElement is an Element that can be exposed as a formal
 * TemplateParameter for a template, or specified as an actual parameter in a
 * binding of a template.
 *
 * @author Maxim Rodyushkin */
public interface ParameterableElement extends Element {

  /** Gets the formal TemplateParameter that owns this ParameterableElement.
   *
   * @return the Template Parameter */
  default Optional<TemplateParameter> getOwningTemplateParameter() {
    return Optional.empty();
  }

  /** Gets the TemplateParameter that exposes this ParameterableElement as a
   * formal parameter.
   *
   * @return the parameter */
  default Optional<TemplateParameter> getTemplateParameter() {
    return this.getOwningTemplateParameter();
  }

  /** Determines if this ParameterableElement is compatible with the specified
   * ParameterableElement.
   *
   * <p>By default, this ParameterableElement is compatible with another
   * ParameterableElement if the kind of this ParameterableElement is the same
   * as or a subtype of the kind of another one. Subclasses of
   * ParameterableElement should override this operation to specify different
   * compatibility constraints.
   *
   * <p>body: self.oclIsKindOf(p.oclType())
   *
   * @param element
   *          the element
   * @return compatibility indicator. It is FALSE by default */
  default UmlBoolean isCompatibleWith(final ParameterableElement element) {
    return UmlBooleanStatic.FALSE;
  }

  /** Determines if this ParameterableElement is exposed as a formal
   * TemplateParameter.
   *
   * <p>body: templateParameter->notEmpty()
   *
   * @return parameter indicator. It is FALSE by default. */
  default UmlBoolean isTemplateParameter() {
    return UmlBooleanStatic.FALSE;
  }
}
