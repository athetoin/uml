package com._4meonweb.uml.commonstructures;

import com._4meonweb.primitivetypes.UmlBoolean;

import java.util.stream.Stream;

/** A Template Signature bundles the set of formal TemplateParameters for a
 * template.
 *
 * @author Maxim Rodyushkin */
public interface TemplateSignature extends Element {
  /** Gets the formal parameters that are owned by this TemplateSignature.
   *
   * @return the formal parameters */
  default <P extends TemplateParameter> Stream<P> getOwnedParameter() {
    return Stream.empty();
  }

  /** Gets the ordered set of all formal TemplateParameters for this
   * TemplateSignature.
   *
   * @return the ordered set of all formal Template Parameters */
  default <P extends TemplateParameter> Stream<P> getParameter() {
    return Stream.empty();
  }

  /** Gets the TemplateableElement that owns this TemplateSignature.
   *
   * @return the Templateable Element */
  default <E extends TemplateableElement> E getTemplate() {
    return null;
  }

  /** Checks that parameters must own the ParameterableElements they parameter
   * or those ParameterableElements must be owned by the TemplateableElement
   * being templated.
   *
   * @return constraint value. It is FALSE by default. */
  UmlBoolean ownElements();

  /** Checks that the names of the parameters of a TemplateSignature are unique.
   *
   * @return constraint value It is FALSE by default. */
  UmlBoolean uniqueParameters();
}
