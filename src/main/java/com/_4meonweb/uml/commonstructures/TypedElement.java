package com._4meonweb.uml.commonstructures;

import java.util.Optional;

/** A TypedElement is a NamedElement that may have a Type specified for it.
 *
 * @author Maxim Rodyushkin */
public interface TypedElement extends NamedElement {

  /** Typed element builder.
   *
   * @author Maxim Rodyushkin
   *
   * @param <E>
   *          the implemented element type
   * @param <M>
   *          the implemented builder type */
  public interface TypedElementMaker<E extends TypedElement,
      M extends TypedElementMaker<E, M>> {

    /** Puts type.
     *
     * @param type
     *          the type
     * @return this builder */
    default M putType(final UmlType type) {
      return null;
    }
  }

  /** The type of the TypedElement.
   *
   * @return the type */
  default <T extends UmlType> Optional<T> getType() {
    return Optional.empty();
  }
}
