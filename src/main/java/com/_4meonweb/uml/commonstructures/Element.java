package com._4meonweb.uml.commonstructures;

import com._4meonweb.primitivetypes.UmlBoolean;
import com._4meonweb.primitivetypes.UmlBooleanStatic;

import java.util.Optional;
import java.util.stream.Stream;

/** An Element is a constituent of a model. As such, it has the capability of
 * owning other Elements.
 *
 * @author Maxim Rodyushkin */
public interface Element extends PrimitivePlugin {

  /** Indicates whether Elements of this type must have an owner. Subclasses of
   * Element that do not require an owner must override this operation.
   *
   * @return owner indicator */
  default UmlBoolean checkMustBeOwned() {
    return UmlBooleanStatic.TRUE;
  }

  /** Gives all of the direct and indirect ownedElements of an Element.
   *
   * <p>body: ownedElement->union(ownedElement->collect(e |
   * e.allOwnedElements()))->asSet()
   *
   * @return the element collection */
  default Stream<Element> getAllOwnedElements() {
    return Stream
        .concat(this.getOwnedElements(),
            this.getOwnedElements().flatMap(Element::getAllOwnedElements))
        .distinct();
  }

  /** Gets the Comments owned by this Element.
   *
   * @return the comment collection */
  default Stream<Comment> getOwnedComments() {
    return Stream.empty();
  }

  /** Gets the Elements owned by this Element.
   *
   * @return the set of elements */
  default Stream<Element> getOwnedElements() {
    return this.getOwnedComments().map(Element.class::cast).distinct();
  }

  /** Gets the Element that owns this Element.
   *
   * @return the element */
  Optional<Element> getOwner();
}
