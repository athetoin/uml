package com._4meonweb.uml.commonstructures;

import com._4meonweb.primitivetypes.UmlBoolean;

import java.util.Optional;
import java.util.stream.Stream;

/** A TemplateableElement is an Element that can optionally be defined as a
 * template and bound to other templates.
 *
 * @author Maxim Rodyushkin */
// TODO Define OCL ancestor
public interface TemplateableElement extends Element {
  public interface TemplateableElementMaker<E extends TemplateableElement,
      M extends TemplateableElementMaker<E, M>> {

  }

  /** Gets the optional TemplateSignature specifying the formal
   * TemplateParameters for this TemplateableElement. If a TemplateableElement
   * has a TemplateSignature, then it is a template.
   *
   * @return the optional Template Signature */
  default <
      T extends TemplateSignature> Optional<T> getOwnedTemplateSignature() {
    return Optional.empty();
  }

  /** Returns the set of ParameterableElements that may be used as the
   * parameteredElements for a TemplateParameter of this TemplateableElement.
   *
   * <p>By default, this set includes all the ownedElements. Subclasses may
   * override this operation if they choose to restrict the set of
   * ParameterableElements.
   *
   * @return the set of parameterable elements */
  default Stream<ParameterableElement> getParameterableElements() {
    return this.getAllOwnedElements()
        .filter(ParameterableElement.class::isInstance)
        .map(ParameterableElement.class::cast);
  }

  /** Gets the optional TemplateBindings from this TemplateableElement to one or
   * more templates.
   *
   * @return template bindings */
  default Stream<TemplateBinding> getTemplateBinding() {
    return Stream.empty();
  }

  /** Returns whether this TemplateableElement is actually a template.
   *
   * @return template indicator. It is FALSE by default. */
  default UmlBoolean isTemplate() {
    throw new UnsupportedOperationException();
  }
}
