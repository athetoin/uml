package com._4meonweb.uml.commonstructures;

import com._4meonweb.primitivetypes.UmlBoolean;

import java.util.Optional;

/** A TemplateParameter exposes a ParameterableElement as a formal parameter of
 * a template.
 *
 * @author Maxim Rodyushkin */
// TODO Define OCL ancestor
public interface TemplateParameter extends Element {
  /** Gets the ParameterableElement that is the default for this formal
   * TemplateParameter.
   *
   * @return the Parameterable Element */
  default <P extends ParameterableElement> Optional<P> getDefault() {
    return Optional.empty();
  }

  /** Gets the ParameterableElement that is owned by this TemplateParameter for
   * the purpose of providing a default.
   *
   * @return the Parameterable Element */
  default <P extends ParameterableElement> Optional<P> getOwnedDefault() {
    return Optional.empty();
  }

  /** Gets the ParameterableElement that is owned by this TemplateParameter for
   * the purpose of exposing it as the parameteredElement.
   *
   * @return the Parameterable Element */
  default <
      P extends ParameterableElement> Optional<P> getOwnedParameteredElement() {
    return Optional.empty();
  }

  /** Gets the ParameterableElement exposed by this TemplateParameter.
   *
   * @return the Parameterable Element */
  default <P extends ParameterableElement> P getParameteredElement() {
    return null;
  }

  /** Gets the TemplateSignature that owns this TemplateParameter.
   *
   * @return the Template Signature */
  default <T extends TemplateSignature> T getSignature() {
    return null;
  }

  /** Checks that the default must be compatible with the formal
   * TemplateParameter.
   *
   * @return constraint value. It is FALSE by default. */
  UmlBoolean mustBeCompatible();
}
