package com._4meonweb.uml.commonstructures;

import com._4meonweb.primitivetypes.UmlBoolean;
import com._4meonweb.primitivetypes.UmlBooleanStatic;
import com._4meonweb.primitivetypes.UmlString;
import com._4meonweb.uml.commonstructures.PackagedElementLink.PackagedElementLinkMaker;

import java.util.Optional;
import java.util.stream.Stream;

/** A package can have one or more profile applications to indicate which
 * profiles have been applied. Because a profile is a package, it is possible to
 * apply a profile not only to packages, but also to profiles. Package
 * specializes TemplateableElement and PackageableElement specializes
 * ParameterableElement to specify that a package can be used as a template and
 * a PackageableElement as a template parameter. A package is used to group
 * elements, and provides a namespace for the grouped elements. */
public interface UmlPackage
    extends PackageableElement, TemplateableElement, Namespace {

  /** UML Package maker interface.
   *
   * @author Maxim Rodyushkin */
  public interface UmlPackageMaker<E extends UmlPackage,
      M extends UmlPackageMaker<E, M>>
      extends UmlPackage, TemplateableElementMaker<E, M> {

    PackagedElementLinkMaker createPackagedElementLinkMaker();

    default <P extends UmlPackage> M putNestingPackage(final P umlPackage) {
      return null;
    }

    /** Puts URI value.
     *
     * @return the builder */
    default <S extends UmlString> M putUri(final S uri) {
      return null;
    }
  }

  @Override
  default UmlBoolean checkMustBeOwned() {
    return UmlBooleanStatic.FALSE;
  }

  /** References the packaged elements that are Packages.
   *
   * @return the set of packages */
  default Stream<UmlPackage> getNestedPackages() {
    return Stream.empty();
  }

  /** References the Package that owns this Package.
   *
   * @return the package */
  default Optional<UmlPackage> getNestingPackage() {
    return Optional.empty();
  }

  @Override
  default Stream<NamedElement> getOwnedMembers() {
    return Stream.concat(Namespace.super.getOwnedMembers(),
        this.getPackagedElements());
  }

  /** References the packaged elements that are Types.
   *
   * @return the set of types */
  default Stream<UmlType> getOwnedTypes() {
    return Stream.empty();
  }

  @Override
  default Optional<Element> getOwner() {
    return Optional.empty();
  }

  /** Specifies the packageable elements that are owned by this Package.
   *
   * @return the set of packageable elements */
  default Stream<PackageableElement> getPackagedElements() {
    return Stream.empty();
  }

  /** Provides an identifier for the package that can be used for many purposes.
   * A URI is the universally unique identification of the package following the
   * IETF URI specification, RFC 2396 http://www.ietf.org/rfc/rfc2396.txt and it
   * must comply with those syntax rules.
   *
   * @return optional package URI */
  @com._4meonweb.uml.commonstructures.constraint.HasUriFormat
  default Optional<UmlString> getUri() {
    return Optional.empty();
  }

}
