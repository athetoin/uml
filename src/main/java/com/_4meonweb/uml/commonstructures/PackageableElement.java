package com._4meonweb.uml.commonstructures;

import java.util.Optional;

/** A PackageableElement is a NamedElement that may be owned directly by a
 * Package. A PackageableElement is also able to serve as the parameteredElement
 * of a TemplateParameter.
 *
 * @author Maxim Rodyushkin */
public interface PackageableElement extends ParameterableElement, NamedElement {

  /** A PackageableElement must have a visibility specified if it is owned by a
   * Namespace. The default visibility is public. */
  @Override
  default Optional<VisibilityKind> getVisibility() {
    return Optional.of(VisibilityKind.PUBLIC);
  }
}
