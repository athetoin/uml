package com._4meonweb.uml.commonstructures;

import com._4meonweb.primitivetypes.UmlBoolean;
import com._4meonweb.primitivetypes.UmlString;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/** A Namespace is an Element in a model that owns and/or imports a set of
 * NamedElements that can be identified by name.
 *
 * @author Maxim Rodyushkin */
public interface Namespace extends NamedElement {

  /** References the ElementImports owned by the Namespace.
   *
   * @return the element imports */
  default Stream<ElementImport> getElementImports() {
    return Stream.empty();
  }

  default Stream<PackageableElement> getExcludedCollisions(
      final Stream<? extends PackageableElement> imps) {
    final var elms = imps.collect(Collectors.toSet());
    return elms.stream()
        .filter(elmt -> elms.stream()
            .allMatch(mmbr -> elmt.equals(mmbr)
                || mmbr.isDistinguishableFrom(elmt, this).getValue()))
        .map(PackageableElement.class::cast);
  }

  /** References the PackageableElements that are members of this Namespace as a
   * result of either PackageImports or ElementImports.
   *
   * @return the packageable elements */
  default Stream<PackageableElement> getImportedMembers() {
    return Stream.empty();
  }

  default Stream<PackageableElement> getImportMembers(
      final Stream<? extends PackageableElement> imps) {
    final var mmbs = this.getMembers().collect(Collectors.toSet());
    return imps
        .filter(elmt -> mmbs.stream().allMatch(
            mmbr -> mmbr.isDistinguishableFrom(elmt, this).getValue()))
        .map(PackageableElement.class::cast);
  }

  default Stream<NamedElement> getMembers() {
    return this.getOwnedMembers();
  }

  default Stream<UmlString> getNamesOfMember(final NamedElement element) {
    return this.getOwnedMembers().anyMatch(element::equals)
        ? Stream.of(element).flatMap(elmt -> elmt.getName().stream())
        : Stream.empty();
  }

  @Override
  default Stream<Element> getOwnedElements() {
    return Stream.concat(NamedElement.super.getOwnedElements(),
        this.getOwnedMembers());
  }

  default Stream<NamedElement> getOwnedMembers() {
    return Stream.empty();
  }

  default Stream<Constraint> getOwnedRules() {
    return Stream.empty();
  }

  default Stream<PackageImport> getPackageImports() {
    return Stream.empty();
  }

  /** Checks whether all of the Namespace's members are distinguishable within
   * it.
   *
   * @return it is FALSE by default */
  default UmlBoolean membersAreDistinguishable() {
    return () -> false;
  }
}
