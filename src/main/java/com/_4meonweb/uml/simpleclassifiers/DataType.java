package com._4meonweb.uml.simpleclassifiers;

import com._4meonweb.uml.classification.Classifier;

/** UML Data Type.
 *
 * @author Maxim Rodyushkin */
public interface DataType extends Classifier {

}
