package com._4meonweb.uml.simpleclassifiers;

/** UML Primitive Type.
 *
 * @author Maxim Rodyushkin */
public interface PrimitiveType extends DataType {
}
