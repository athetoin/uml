package com._4meonweb.uml.commonstructures.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.junit.jupiter.api.DynamicContainer.dynamicContainer;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;
import static org.mockito.Mockito.mock;

import com._4meonweb.primitivetypes.UmlString;
import com._4meonweb.uml.commonstructures.Element;
import com._4meonweb.uml.commonstructures.NamedElement;
import com._4meonweb.uml.commonstructures.Namespace;
import com._4meonweb.uml.commonstructures.UmlPackage;

import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicNode;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;

public interface NamedElementBond extends ElementBond {

  interface NamedWithAttributes {
    NamedElement getNamedElement();

    Optional<String> getName();
  }

  default DynamicTest checkHasNoQualifiedName_Ever_True(
      final NamedElement elmt) {
    return dynamicTest("checkHasNoQualifiedName_Ever_True", () -> {
      assumeTrue(elmt.getName().isEmpty() || elmt.getAllNamespaces()
          .flatMap(nmsc -> nmsc.getName().stream()).anyMatch(Objects::isNull));
      assertTrue(elmt.getQualifiedName().isEmpty());
    });
  }

  default DynamicTest checkHasQualifiedName_Ever_True(final NamedElement elmt) {
    return dynamicTest("checkHasQualifiedName_Ever_True", () -> {
      assumeTrue(elmt.getName().isPresent());
      assumeTrue(elmt.getAllNamespaces()
          .flatMap(nmsc -> nmsc.getName().stream()).allMatch(Objects::nonNull));
      assertEquals(elmt.getQualifiedName().get().getValue(),
          Stream
              .concat(
                  elmt.getAllNamespaces().map(NamedElement::getName)
                      .flatMap(Optional::stream).map(UmlString::getValue),
                  Stream.of(elmt.getName().get().getValue()))
              .collect(Collectors.joining(elmt.getSeparator().getValue())));
    });
  }

  @TestFactory
  @DisplayName("Named Element Check")
  default Stream<DynamicNode> checkNamedElement() {
    return this.getNamedElements().map(NamedWithAttributes::getNamedElement)
        .map(elmt -> dynamicContainer(elmt.toString(), Stream.concat(
            Stream.of(this.checkHasNoQualifiedName_Ever_True(elmt),
                this.checkHasQualifiedName_Ever_True(elmt),
                this.checkVisibilityNeedsOwnership_Ever_True(elmt),
                this.getAllNamespaces_HasNamespace_First(elmt),
                this.getAllOwningPackages_NamespaceIsPackage_Contains(elmt),
                this.getAllOwningPackages_NamespaceOwnedByPackages_Contains(
                    elmt),
                this.getQualifiedName_HasName_Contained(elmt),
                this.getQualifiedName_HasNamespaceQualifiedName_Contained(elmt),
                this.getQualifiedName_HasValue_ContainedSeparator(elmt),
                this.getSeparator_Ever_Constant(elmt),
                this.isDitinguashableFrom_ItselfInNamespace_False(elmt),
                this.isDitinguashableFrom_MockedElement_True(elmt)),
            Stream.of(dynamicContainer("getName_HasName_Matched",
                this.getNamedElements()
                    .filter(nmd -> nmd.getNamedElement().equals(elmt))
                    .map(NamedWithAttributes::getName)
                    .filter(Optional::isPresent).map(Optional::get)
                    .map(nm -> this.getName_HasName_Matched(elmt, nm)))))));
  }

  default DynamicTest checkVisibilityNeedsOwnership_Ever_True(
      final NamedElement elmt) {
    return dynamicTest("checkVisibilityNeedsOwnership_Ever_True", () -> {
      assumeTrue(elmt.getNamespace().isEmpty());
      assumeTrue(elmt.getOwner().isPresent());
      assertTrue(elmt.getVisibility().isEmpty());
    });
  }

  default void chkQNameUse(final NamedElement elmt) {
    assumeTrue(Objects.nonNull(elmt.getQualifiedName()));
    assumeTrue(Objects.nonNull(elmt.getName()));
    assumeTrue(Objects.nonNull(elmt.getNamespace()));
    assumeTrue(elmt.getName().isPresent());
    assumeTrue(elmt.getNamespace().isPresent());
  }

  @Override
  default Stream<AccessorWithRelations> getAccessors() {
    final String[][][] prps = {
        // Attributes
        { { "getName" } }, { { "getQualifiedName" } }, { { "getVisibility" } },
        // Association ends
        { { "getClientDependencies" }, {}, { "getClient" } },
        { { "getNameExpression" }, { "getOwnedElements" }, { "getOwner" } },
        { { "getNamespace" }, { "getOwner" }, { "getOwnedMembers" } },
        // Methods
        { { "getAllNamespaces" } }, { { "getAllOwningPackages" } },
        { { "getSeparator" } }, };

    return Stream.concat(ElementBond.super.getAccessors(),
        this.convertAccessors(prps));
  }

  default DynamicTest getAllNamespaces_HasNamespace_First(
      final NamedElement elmt) {
    return dynamicTest("getAllNamespaces_HasNamespace_First", () -> {
      assumeTrue(Objects.nonNull(elmt.getAllNamespaces()));
      assumeTrue(Objects.nonNull(elmt.getNamespace().isPresent()));
      assertEquals(elmt.getNamespace(), elmt.getAllNamespaces().findFirst());
    });
  }

  default DynamicTest getAllOwningPackages_NamespaceIsPackage_Contains(
      final NamedElement elmt) {
    return dynamicTest("getAllOwningPackages_NamespaceIsPackage_Contains",
        () -> {
          assumeTrue(Objects.nonNull(elmt.getAllOwningPackages()));
          assumeTrue(Objects.nonNull(elmt.getNamespace()));
          assumeTrue(elmt.getNamespace().isPresent());
          assumeTrue(elmt.getNamespace().filter(UmlPackage.class::isInstance)
              .isPresent());
          assertTrue(elmt.getNamespace()
              .filter(
                  nmsc -> elmt.getAllOwningPackages().anyMatch(nmsc::equals))
              .isPresent());
        });
  }

  default DynamicTest getAllOwningPackages_NamespaceOwnedByPackages_Contains(
      final NamedElement elmt) {
    return dynamicTest("getAllOwningPackages_NamespaceOwnedByPackages_Contains",
        () -> {
          assumeTrue(Objects.nonNull(elmt.getAllOwningPackages()));
          assumeTrue(Objects.nonNull(elmt.getNamespace()));
          assumeTrue(elmt.getNamespace().isPresent());
          assumeTrue(elmt.getNamespace().filter(UmlPackage.class::isInstance)
              .isPresent());
          assertTrue(elmt.getNamespace().stream()
              .flatMap(NamedElement::getAllOwningPackages)
              .allMatch(nmscPckg -> elmt.getAllOwningPackages()
                  .anyMatch(nmscPckg::equals)));
        });
  }

  @Override
  default Stream<Element> getElements() {
    return this.getNamedElements().map(NamedWithAttributes::getNamedElement);
  }

  default DynamicTest getName_HasName_Matched(final NamedElement elmt,
      final String name) {
    return dynamicTest(elmt + " to <" + name + ">", () -> {
      assumeTrue(Objects.nonNull(elmt.getName()));
      assumeTrue(elmt.getName().isPresent());
      assertEquals(Optional.of(name), elmt.getName().map(UmlString::getValue));
    });
  }

  Stream<NamedWithAttributes> getNamedElements();

  default DynamicTest getQualifiedName_HasName_Contained(
      final NamedElement elmt) {
    return dynamicTest("getQualifiedName_HasName_Contained", () -> {
      assumeTrue(elmt.getName().isPresent());
      assertTrue(elmt.getQualifiedName().map(UmlString::getValue)
          .filter(strg -> strg.contains(elmt.getName().get().getValue()))
          .isPresent());
    });
  }

  default DynamicTest getQualifiedName_HasNamespaceQualifiedName_Contained(
      final NamedElement elmt) {
    return dynamicTest("getQualifiedName_HasNamespaceQualifiedName_Contained",
        () -> {
          this.chkQNameUse(elmt);
          assumeTrue(
              elmt.getNamespace().flatMap(Namespace::getName).isPresent());
          assertTrue(elmt.getQualifiedName().map(UmlString::getValue)
              .filter(strg -> strg.contains(elmt.getNamespace()
                  .flatMap(NamedElement::getName).get().getValue()))
              .isPresent());
        });
  }

  default DynamicTest getQualifiedName_HasValue_ContainedSeparator(
      final NamedElement elmt) {
    return dynamicTest("getQualifiedName_HasValue_ContainedSeparator", () -> {
      this.chkQNameUse(elmt);
      assumeTrue(elmt.getNamespace().flatMap(Namespace::getName).isPresent());
      assertTrue(elmt.getQualifiedName().map(UmlString::getValue)
          .filter(strg -> strg.contains(elmt.getSeparator().getValue()))
          .isPresent());
    });
  }

  default DynamicTest getSeparator_Ever_Constant(final NamedElement elmt) {
    return dynamicTest("getSeparator_Ever_Constant",
        () -> assertEquals("::", elmt.getSeparator().getValue()));
  }

  default DynamicTest isDitinguashableFrom_ItselfInNamespace_False(
      final NamedElement elmt) {
    return dynamicTest("isDitinguashableFrom_ItselfInNamespace_False", () -> {
      assumeTrue(Objects.nonNull(elmt.getNamespace()));
      assumeTrue(elmt.getNamespace().isPresent());
      assumeTrue(Objects.nonNull(
          elmt.isDistinguishableFrom(elmt, elmt.getNamespace().get())));
      assertFalse(elmt.isDistinguishableFrom(elmt, elmt.getNamespace().get())
          .getValue());
    });
  }

  default DynamicTest isDitinguashableFrom_MockedElement_True(
      final NamedElement elmt) {
    return dynamicTest("isDitinguashableFrom_MockedElement_True", () -> {
      assumeTrue(elmt.getNamespace().isPresent());
      assertTrue(elmt.isDistinguishableFrom(mock(NamedElement.class),
          elmt.getNamespace().get()).getValue());
    });
  }
}
