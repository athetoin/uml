package com._4meonweb.uml.commonstructures.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.junit.jupiter.api.DynamicContainer.dynamicContainer;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com._4meonweb.primitivetypes.UmlString;
import com._4meonweb.uml.commonstructures.NamedElement;
import com._4meonweb.uml.commonstructures.Namespace;
import com._4meonweb.uml.commonstructures.PackageableElement;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicNode;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;

public interface NamespaceBond extends NamedElementBond {
  interface NamespaceWithAttributes extends NamedWithAttributes {
    @Override
    default NamedElement getNamedElement() {
      return this.getNamespace();
    }

    Namespace getNamespace();
  }

  default DynamicTest checkMembersDistinguishable_Ever_True(
      final Namespace nmsc) {
    return dynamicTest("checkMembersDistinguishable_Ever_True", () -> {
      final var mmbs = nmsc.getMembers().collect(Collectors.toSet());
      assertTrue(mmbs.stream().allMatch(mmbr -> {
        final var mmbs0 = Set.copyOf(mmbs);
        mmbs0.remove(mmbr);
        return mmbs0.stream().allMatch(
            mmbr0 -> mmbr.isDistinguishableFrom(mmbr0, nmsc).getValue());
      }));
    });
  }

  @TestFactory
  @DisplayName("Namespace Check")
  default Stream<DynamicNode> checkNamespace() {
    return this.getNamespaceWithAttributes()
        .map(NamespaceWithAttributes::getNamespace)
        .map(nmsc -> dynamicContainer(nmsc.toString(),
            Stream.of(this.getNamesOfMember_Owned_Includes(nmsc),
                this.getImportMembers_Owned_Excluded(nmsc),
                this.getImportMembers_Mock_NoChange(nmsc),
                this.getImportMembers_DuplicateName_Excluded(nmsc),
                this.getExcludeCollisions_Duplicate_Excluded(nmsc))));
  }

  @Override
  default Stream<AccessorWithRelations> getAccessors() {
    final String[][][] prps = {
        // Association ends
        { { "getElementImports" }, { "getOwnedElements" },
            { "getImportingElement" } },
        { { "getImportedMembers" }, { "getMembers" } },
        { { "getOwnedMembers" }, { "getMembers", "getOwnedElements" },
            { "getNamespace" } },
        { { "getOwnedRules" }, { "getOwnedMembers" }, { "getContext" } },
        { { "getPackageImports" }, { "getOwnedElements" },
            { "getImportingElement" } }, };
    return Stream.concat(NamedElementBond.super.getAccessors(),
        this.convertAccessors(prps));
  }

  default DynamicTest getExcludeCollisions_Duplicate_Excluded(
      final Namespace nmsc) {
    return dynamicTest("getExcludeCollisions_Duplicate_Excluded", () -> {

      final var dplt1 = new PackageableElement() {
        @Override
        public Optional<UmlString> getName() {
          return Optional.of(NamespaceBond.this.getStringMaker().ofThe("1"));
        }
      };

      final var dplt2 = new PackageableElement() {
        @Override
        public Optional<UmlString> getName() {
          return Optional.of(NamespaceBond.this.getStringMaker().ofThe("1"));
        }
      };

      final var uniq = new PackageableElement() {
        @Override
        public Optional<UmlString> getName() {
          return Optional.of(NamespaceBond.this.getStringMaker().ofThe("3"));
        }
      };
      assumeTrue(false, "Cannot create duplicate");
      assertEquals(Stream.of(uniq).collect(Collectors.toSet()),
          nmsc.getExcludedCollisions(Stream.of(dplt1, dplt2, uniq))
              .collect(Collectors.toSet()));
    });
  }

  default DynamicTest getImportMembers_DuplicateName_Excluded(
      final Namespace nmsc) {
    return dynamicTest("getImportMembers_DuplicateName_Excluded", () -> {
      final var dplt1 = mock(PackageableElement.class);
      when(dplt1.getName())
          .thenReturn(Optional.of(this.getStringMaker().ofThe("1")));
      final var dplt2 = mock(PackageableElement.class);
      when(dplt2.getName())
          .thenReturn(Optional.of(this.getStringMaker().ofThe("1")));
      assumeTrue(false, "Cannot create duplicate");
      assertEquals(Collections.emptySet(),
          nmsc.getImportMembers(Stream.of(dplt1, dplt2))
              .collect(Collectors.toSet()));
    });
  }

  default DynamicTest getImportMembers_Mock_NoChange(final Namespace nmsc) {
    return dynamicTest("getImportMembers_Mock_NoChange", () -> {
      final var pckl = mock(PackageableElement.class);
      final var rslt = Stream.of(pckl).collect(Collectors.toSet());
      assertEquals(rslt,
          nmsc.getImportMembers(Stream.of(pckl)).collect(Collectors.toSet()));
    });
  }

  default DynamicTest getImportMembers_Owned_Excluded(final Namespace nmsc) {
    return dynamicTest("getImportMembers_Owned_Excluded",
        () -> assertTrue(
            nmsc.getOwnedMembers().filter(PackageableElement.class::isInstance)
                .map(PackageableElement.class::cast).allMatch(mmbr -> nmsc
                    .getImportMembers(Stream.of(mmbr)).findAny().isEmpty())));
  }

  @Override
  default Stream<NamedWithAttributes> getNamedElements() {
    return this.getNamespaceWithAttributes()
        .map(NamedWithAttributes.class::cast);
  }

  default DynamicTest getNamesOfMember_Owned_Includes(final Namespace nmsc) {
    return dynamicTest("getNamesOfMember_Owned_Includes",
        () -> assertTrue(
            nmsc.getOwnedMembers().allMatch(mmbr -> nmsc.getNamesOfMember(mmbr)
                .anyMatch(name -> mmbr.getName().get().equals(name)))));
  }

  Stream<NamespaceWithAttributes> getNamespaceWithAttributes();
}
