package com._4meonweb.uml.commonstructures.test;

import java.util.stream.Stream;

public interface PackageableElementBond
    extends NamedElementBond, ParameterableElementBond {

  @Override
  default Stream<AccessorWithRelations> getAccessors() {
    return Stream.concat(NamedElementBond.super.getAccessors(),
        ParameterableElementBond.super.getAccessors()).distinct();
  }
}
