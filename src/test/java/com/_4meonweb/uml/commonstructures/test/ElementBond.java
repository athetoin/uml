package com._4meonweb.uml.commonstructures.test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.junit.jupiter.api.DynamicContainer.dynamicContainer;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;

import com._4meonweb.primitivetypes.UmlString.UmlStringMaker;
import com._4meonweb.uml.commonstructures.Element;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicNode;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;

public interface ElementBond {
  interface AccessorWithRelations {
    String getName();

    default Optional<String> getOpposite() {
      return Optional.empty();
    }

    default Stream<String> getSubsetted() {
      return Stream.empty();
    }
  }

  @TestFactory
  @DisplayName("Common Check")
  default Stream<DynamicNode> checkCommonRules() {
    return this.getElements()
        .map(elmt -> dynamicContainer(elmt.toString(), Stream.of(
            dynamicContainer("Ever not NULL",
                this.getProperty_Ever_NotNull(elmt)),
            dynamicContainer("Ever Subset", this.getProperty_Ever_Subset(elmt)),
            dynamicContainer("Ever Opposite",
                this.getProperty_Ever_Opposite(elmt)))));
  }

  @TestFactory
  @DisplayName("Element Check")
  default Stream<DynamicNode> checkElement() {
    return this.getElements()
        .map(elmt -> dynamicContainer(elmt.toString(),
            Stream.of(this.getAllOwnedElements_Ever_NotOwnSelf(elmt),
                this.getAllOwnedElements_HasOwner_OwnerHasAllOwned(elmt),
                this.getOwner_MustBeOwned_hasOwner(elmt))));
  }

  default Stream<AccessorWithRelations> convertAccessors(
      final String[][][] accessors) {
    return Arrays.asList(accessors).stream()
        .map(arr -> new AccessorWithRelations() {

          @Override
          public String getName() {
            return arr[0][0];
          }

          @Override
          public Optional<String> getOpposite() {
            if (arr.length > 2 && arr[2] != null) {
              return Optional.ofNullable(arr[2][0]);
            }
            return Optional.empty();
          }

          @Override
          public Stream<String> getSubsetted() {
            if (arr.length > 1 && arr[1] != null) {
              return Arrays.asList(arr[1]).stream();
            }
            return Stream.empty();
          }
        });
  }

  default Stream<AccessorWithRelations> getAccessors() {
    final String[][][] prps = { { { "getAllOwnedElements" } },
        { { "getOwnedComments" }, { "getOwnedElements" }, { "getOwner" } },
        { { "getOwnedElements" }, {}, { "getOwner" } },
        { { "getOwner" }, {}, { "getOwnedElements" } } };

    return this.convertAccessors(prps);
  }

  default DynamicTest getAllOwnedElements_Ever_NotOwnSelf(final Element elmt) {
    return dynamicTest("getAllOwnedElements_Ever_NotOwnSelf", () -> {
      assumeTrue(Objects.nonNull(elmt.getAllOwnedElements()));
      assertTrue(elmt.getAllOwnedElements().noneMatch(elmt::equals));
    });
  }

  default DynamicTest getAllOwnedElements_HasOwner_OwnerHasAllOwned(
      final Element elmt) {
    return dynamicTest("getAllOwnedElements_HasOwner_OwnerHasAllOwned", () -> {
      assumeTrue(Objects.nonNull(elmt.getAllOwnedElements()));
      assumeTrue(Objects.nonNull(elmt.getOwner()));
      assumeTrue(elmt.getOwner().isPresent());
      assumeTrue(Objects.nonNull(elmt.getOwnedElements()));
      assertTrue(
          elmt.getOwnedElements().allMatch(ownd -> elmt.getOwner().stream()
              .flatMap(Element::getAllOwnedElements).anyMatch(ownd::equals)));
    });
  }

  Stream<Element> getElements();

  default DynamicTest getOwner_MustBeOwned_hasOwner(final Element elmt) {
    return dynamicTest("getOwner_MustBeOwned_hasOwner", () -> {
      assumeTrue(Objects.nonNull(elmt.getOwner()));
      assumeTrue(elmt.checkMustBeOwned().getValue());
      assertTrue(elmt.getOwner().isPresent());
    });
  }

  default <E extends Element> Stream<DynamicTest> getProperty_Ever_NotNull(
      final E nmdt) {
    final var nms = this.getAccessors().map(AccessorWithRelations::getName)
        .collect(Collectors.toList());
    return Stream.of(nmdt.getClass().getMethods())
        .filter(mthd -> nms.contains(mthd.getName()))
        .map(mthd -> dynamicTest(mthd.getName(),
            () -> assertNotNull(mthd.invoke(nmdt))));
  }

  default <E extends Element> Stream<DynamicTest> getProperty_Ever_Opposite(
      final E nmdt) {
    final var nms = this.getAccessors()
        .flatMap(acsr -> acsr.getOpposite().filter(Objects::nonNull).stream()
            .map(sbtd -> new String[] { acsr.getName(), sbtd }));
    final var mthd = Stream.of(nmdt.getClass().getMethods())
        .filter(mthd0 -> mthd0.getParameterCount() == 0)
        .collect(Collectors.toMap(Method::getName, mthd0 -> mthd0));
    return nms
        .map(acsr -> dynamicTest(acsr[0] + " opposite to " + acsr[1], () -> {
          final var tstdRslt = mthd.get(acsr[0]).invoke(nmdt);
          final Function<Object, Stream<?>> rsltToStrm =
              (rslt) -> Stream.of((Optional.class.isInstance(rslt)
                  ? Optional.class.cast(rslt).stream()
                  : Stream.class.cast(rslt)).toArray());
          rsltToStrm.apply(tstdRslt).map((tstd) -> {
            try {
              final var tstdMthd = Stream.of(tstd.getClass().getMethods())
                  .filter(mthd0 -> mthd0.getName().equals(acsr[1])).findFirst()
                  .get();
              final var opst = tstdMthd.invoke(tstd);
              return rsltToStrm.apply(opst);
            } catch (IllegalAccessException | IllegalArgumentException
                | InvocationTargetException e) {
              throw new IllegalCallerException(e);
            }
          }).forEach(opstStrm -> assertTrue(opstStrm.anyMatch(nmdt::equals)));
        }));
  }

  default <E extends Element> Stream<DynamicTest> getProperty_Ever_Subset(
      final E nmdt) {
    final var nms = this.getAccessors().flatMap(acsr -> acsr.getSubsetted()
        .map(sbtd -> new String[] { acsr.getName(), sbtd }));
    final var mthd = Stream.of(nmdt.getClass().getMethods())
        .filter(mthd0 -> mthd0.getParameterCount() == 0)
        .collect(Collectors.toMap(Method::getName, mthd0 -> mthd0));
    return nms
        .map(acsr -> dynamicTest(acsr[0] + " subset of " + acsr[1], () -> {
          final var sbstRslt = mthd.get(acsr[0]).invoke(nmdt);
          final var unionRslt = mthd.get(acsr[1]).invoke(nmdt);
          final Function<Object, Stream<?>> rsltToArr =
              (rslt) -> Stream.of((Optional.class.isInstance(rslt)
                  ? Optional.class.cast(rslt).stream()
                  : Stream.class.cast(rslt)).toArray());
          final var subset = rsltToArr.apply(sbstRslt);
          final var union =
              rsltToArr.apply(unionRslt).collect(Collectors.toList());
          assertTrue(
              subset.allMatch(objt -> union.stream().anyMatch(objt::equals)));
        }));
  }

  UmlStringMaker getStringMaker();
}
