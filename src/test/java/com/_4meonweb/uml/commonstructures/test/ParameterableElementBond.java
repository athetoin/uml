package com._4meonweb.uml.commonstructures.test;

import java.util.stream.Stream;

public interface ParameterableElementBond extends ElementBond {

  @Override
  default Stream<AccessorWithRelations> getAccessors() {
    return ElementBond.super.getAccessors();
  }

}
