package com._4meonweb.uml.commonstructures.test;

import java.util.stream.Stream;

public interface UmlPackageBond
    extends NamespaceBond, PackageableElementBond, TemplateableElementBond {

  @Override
  default Stream<AccessorWithRelations> getAccessors() {
    return Stream.concat(
        Stream.concat(NamespaceBond.super.getAccessors(),
            PackageableElementBond.super.getAccessors()),
        TemplateableElementBond.super.getAccessors()).distinct();
  }

}
