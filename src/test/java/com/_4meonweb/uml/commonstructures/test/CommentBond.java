package com._4meonweb.uml.commonstructures.test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;

import com._4meonweb.primitivetypes.UmlString;
import com._4meonweb.uml.commonstructures.Comment;
import com._4meonweb.uml.commonstructures.Element;

import java.util.Optional;
import java.util.stream.Stream;

import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;

public interface CommentBond extends ElementBond {

  interface CommentWithAttributes {
    Stream<Element> getAnnotated();

    Optional<String> getBody();

    Comment getComment();
  }

  @Override
  default Stream<AccessorWithRelations> getAccessors() {
    final String[][][] prps = {
        // Attributes
        { { "getBody" } },
        // Association ends
        { { "getAnnotatedElements" } },
        // Methods
    };

    return Stream.concat(ElementBond.super.getAccessors(),
        this.convertAccessors(prps));
  }

  /** Tests expected annotated element. */
  @TestFactory
  default Stream<DynamicTest> getAnnotatedElement_Ever_Match() {
    return this.getCommentsWithAttributes()
        .map(atts -> dynamicTest(
            atts.getComment() + " with " + atts.getAnnotated(),
            () -> assertArrayEquals(atts.getAnnotated().toArray(),
                atts.getComment().getAnnotatedElements().toArray(),
                "Mismatched annotated element")));
  }

  /** Tests Primitive comment body. */
  @TestFactory
  default Stream<DynamicTest> getBody_Ever_Match() {
    return this.getCommentsWithAttributes()
        .map(cmnt -> dynamicTest(cmnt.getComment() + " with " + cmnt.getBody(),
            () -> assertEquals(cmnt.getBody(),
                cmnt.getComment().getBody().map(UmlString::getValue))));
  }

  /** Gets tested Comments with attributes.
   *
   * @return the comments */
  Stream<CommentWithAttributes> getCommentsWithAttributes();

  @Override
  default Stream<Element> getElements() {
    return this.getCommentsWithAttributes()
        .map(CommentWithAttributes::getComment).map(Element.class::cast);
  }
}
