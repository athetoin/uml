package com._4meonweb.uml.classification.test;

import com._4meonweb.uml.commonstructures.test.NamespaceBond;
import com._4meonweb.uml.commonstructures.test.TemplateableElementBond;
import com._4meonweb.uml.commonstructures.test.UmlTypeBond;

import java.util.stream.Stream;

public interface ClassifierBond extends RedefinableElementBond, NamespaceBond,
    UmlTypeBond, TemplateableElementBond {

  @Override
  default Stream<AccessorWithRelations> getAccessors() {
    return Stream.concat(RedefinableElementBond.super.getAccessors(),
        NamespaceBond.super.getAccessors());
  }

  @Override
  default Stream<NamedWithAttributes> getNamedElements() {
    // TODO Auto-generated method stub
    return NamespaceBond.super.getNamedElements();
  }

}
