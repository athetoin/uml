package com._4meonweb.uml.classification.test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.DynamicContainer.dynamicContainer;
import static org.junit.jupiter.api.DynamicTest.dynamicTest;
import static org.mockito.Mockito.mock;

import com._4meonweb.primitivetypes.UmlBoolean;
import com._4meonweb.uml.classification.RedefinableElement;
import com._4meonweb.uml.commonstructures.NamedElement;
import com._4meonweb.uml.commonstructures.test.NamedElementBond;

import java.util.stream.Stream;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DynamicNode;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;

public interface RedefinableElementBond extends NamedElementBond {
  interface RedefinableWithAttributes extends NamedWithAttributes {
    @Override
    default NamedElement getNamedElement() {
      return this.getRedefinableElement();
    }

    RedefinableElement getRedefinableElement();
  }

  @Override
  @TestFactory
  @DisplayName("Redefinable Element Check")
  default Stream<DynamicNode> checkNamedElement() {
    return this.getRedefinableElements()
        .map(RedefinableWithAttributes::getRedefinableElement)
        .map(elmt -> dynamicContainer(elmt.toString(),
            Stream.of(this.checkNonLeafRedefinition_Ever_True(elmt),
                this.isRedefinitionContextvalid_Mocked_False(elmt))));
  }

  default DynamicTest checkNonLeafRedefinition_Ever_True(
      final RedefinableElement elmt) {
    return dynamicTest("checkNonLeafRedefinition_Ever_True", () -> {
      assertTrue(elmt.getRedefinedElements().map(RedefinableElement::isLeaf)
          .map(UmlBoolean::getValue).allMatch(prdt -> prdt));
    });
  }

  @Override
  default Stream<AccessorWithRelations> getAccessors() {
    final String[][][] prps = {
        // Attributes
        { { "isLeaf" } },
        // Association ends
        { { "getRedefinedElements" } }, { { "getRedefinitionContexts" } },
        // Operations
    };
    return Stream.concat(NamedElementBond.super.getAccessors(),
        this.convertAccessors(prps));
  }

  @Override
  default Stream<NamedWithAttributes> getNamedElements() {
    return this.getRedefinableElements().map(NamedWithAttributes.class::cast);
  }

  Stream<RedefinableWithAttributes> getRedefinableElements();

  default DynamicTest isRedefinitionContextvalid_Mocked_False(
      final RedefinableElement elmt) {
    return dynamicTest("isRedefinitionContextValid_Mocked_False", () -> {
      assertFalse(
          elmt.isRedefinitionContextValid(mock(RedefinableElement.class))
              .getValue());
    });
  }
}
